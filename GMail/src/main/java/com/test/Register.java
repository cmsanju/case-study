package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("fname");
		String usr = request.getParameter("user");
		String pas = request.getParameter("pwd");
		
		User u = new User();
		
		u.setFullName(name);
		u.setUserName(usr);
		u.setPassword(pas);
		
		UserDao udao = new UserDao();
		
		udao.save(u);
		
		if(usr != null && pas != null)
		{
			RequestDispatcher rd = request.getRequestDispatcher("login.html");
			
			rd.forward(request, response);
		}
		else
		{
			RequestDispatcher rd = request.getRequestDispatcher("reg.html");
			rd.include(request, response);
		}
	}

}
