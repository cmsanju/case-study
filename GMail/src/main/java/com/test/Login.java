package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String usr = request.getParameter("user");
		String pas = request.getParameter("pwd");
		
		User u = new User();
		
		u.setUserName(usr);
		
		UserDao udao = new UserDao();
		
		User dbData =  udao.readUser(u);
		
		
		
		if(usr.equals(dbData.getUserName()) && pas.equals(dbData.getPassword()))
		{
			//RequestDispatcher rd = request.getRequestDispatcher("home.html");
			//rd.forward(request, response);
			//response.sendRedirect("home.html");
			response.sendRedirect("https://www.google.com/");
		}
		else
		{
			out.println("<center><font color='red'>invalid credentials</font></center>");
			
			RequestDispatcher rd = request.getRequestDispatcher("login.html");
			rd.include(request, response);
		}
	}

}
