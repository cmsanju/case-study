package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserDao {
	
	public static Connection getConnection()
	{
		Connection con = null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/plm", "root", "password");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return con;
	}
	
	public void save(User u)
	{
		try {
		String sql = "insert into user1 (full_name, username, u_password) values('"+u.getFullName()+"','"+u.getUserName()+"', '"+u.getPassword()+"')";
		
		Statement stmt =  getConnection().createStatement();
		
		stmt.execute(sql);
		
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public User readUser(User u)
	{
		User us = null;
		try
		{
			String sql = "select username, u_password from user1 where username = '"+u.getUserName()+"'";
			
			Statement stmt = getConnection().createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			 us = new User();
			while(rs.next())
			{
				
				
				us.setUserName(rs.getString(1));
				us.setPassword(rs.getString(2));
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return us;
	}
}
