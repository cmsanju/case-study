<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
		<h1>JSP Tags</h1>
		
		<!-- declaration tag -->
		
		<%!
			int x = 20;
			int y = 30;
			
			public int add()
			{
				return x+y;
			}
		
		%>
		
		<!-- expression tag -->
		
		<%= add() %>
		
		<!-- scriplet tag -->
		
		<%
			out.println("Current Date & Time : "+new Date());
		%>
</body>
</html>