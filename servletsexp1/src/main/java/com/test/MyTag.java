package com.test;

import java.util.Date;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class MyTag extends TagSupport
{
	private static final long serialVersionUID = 1L;

	public int doStartTag()
	{
		JspWriter out = null;
		
		try
		{
			out = pageContext.getOut();
			
			out.println("Current Date : "+new Date());
		}
		catch (Exception e ) {
			e.printStackTrace();
		}
		
		
		return SKIP_BODY;
	}
}
