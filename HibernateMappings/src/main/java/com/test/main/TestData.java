package com.test.main;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.test.entity.Flights;
import com.test.entity.User;

public class TestData {
	
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();
		
		Session session = sf.openSession();
		
		Transaction ts = session.beginTransaction();
		
		Flights obj1 = new Flights();
		
		obj1.setFlt_name("Vistara airs");
		obj1.setFlt_cmp("TATA");
		
		Flights obj2 = new Flights();
		
		obj2.setFlt_name("Indigo india");
		obj2.setFlt_cmp("Indigo");
		
		ArrayList<Flights> list = new ArrayList<>();
		
		list.add(obj1);
		list.add(obj2);
		
		User uobj = new User();
		
		uobj.setUser_name("BALEN");
		uobj.setFlights(list);
		
		session.persist(uobj);
		
		ts.commit();
		
		System.out.println("Store Data");
	}
}
