package com.test.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "flights")
public class Flights {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String flt_name;
	
	private String flt_cmp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFlt_name() {
		return flt_name;
	}

	public void setFlt_name(String flt_name) {
		this.flt_name = flt_name;
	}

	public String getFlt_cmp() {
		return flt_cmp;
	}

	public void setFlt_cmp(String flt_cmp) {
		this.flt_cmp = flt_cmp;
	}
}
