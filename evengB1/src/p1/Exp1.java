package p1;

public class Exp1 {
	
	private int a = 101;
	
	        int b = 33;
	        
	protected int c = 21;
	
	public int d = 494;
	
	
	public void disp()
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
	
	public static void main(String[] args) {
		
		Exp1 obj = new Exp1();
		
		obj.disp();
	}
}

class ExpA extends Exp1
{
	public void disp()
	{
		//System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
}
