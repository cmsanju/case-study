package com.strs;

public class Exp1 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str3 = "java";
		
		String str5 = "hello";
		
		String str2 = new String("java");
		String str4 = new String("java");
		
		System.out.println(str1 == str4);
		
		System.out.println(str1.equals(str4));
		
		System.out.println(str2 == str4);
		
		System.out.println(str2.equals(str4));
		
		str1.concat(" developer ");
		
		System.out.println(str1);
		
		System.out.println(str1.charAt(0));
		
		StringBuffer sb = new StringBuffer(str1);
		
		sb.append(" developer");
		
		System.out.println(sb);
		
		sb.insert(2, "update");
		
		System.out.println(sb);
		
		char[] car = str1.toCharArray();
		
		System.out.println(sb.reverse());
	}
}
