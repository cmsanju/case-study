package com.strs;

import java.lang.reflect.Method;

public class TestDemo {
	
	@ValidateInput(value = 777)
	public void dispData()
	{
		System.out.println("Tested");
	}
	
	public static void main(String[] args) throws Exception
	{
		
		TestDemo obj = new TestDemo();
		
		Method robj = obj.getClass().getMethod("dispData");
		
		ValidateInput vobj = robj.getAnnotation(ValidateInput.class);
		
		System.out.println("Input value : "+vobj.value());
	}
}
