package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



public class Exp2 {
	
	public static void main(String[] args)throws Exception 
	{
		
		// load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		// Create connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sales", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into employee11 values(?,?,?)");
		
		pst.setInt(1, 9);
		pst.setString(2, "King");
		pst.setInt(3, 734);
		*/
		
		PreparedStatement ps = con.prepareStatement("update employee11 set emp_name=?, salary = ? where id=?");
		
		ps.setString(1, "King");
		ps.setInt(2, 567);
		ps.setInt(3, 8);
		
		ps.execute();
		
		PreparedStatement pst = con.prepareStatement("select * from employee11");
		
		ResultSet rs = pst.executeQuery();
		
		while(rs.next()) {
		
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" Salary : "+rs.getInt(3));
		}
	}
}
