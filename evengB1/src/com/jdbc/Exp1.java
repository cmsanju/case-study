package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		
		//step 1 load the driver class
		Class.forName("com.mysql.jdbc.Driver");
		
		//step 2 crate connection object
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sales", "root", "password");
		
		//step 3 create statement object
		Statement stmt = con.createStatement();
		
		//step 4 execute query
		//stmt.execute("create table employee11(id int, emp_name varchar(50), salary int)");
		
		String sql1 = "insert into employee11 values(3, 'Kushal', 233)";
		String sql3 = "insert into employee11 values(4, 'Nilesh', 112)";
		String sql4 = "insert into employee11 values(5, 'Samrat', 9339)";
		String sql5 = "insert into employee11 values(6, 'Halesh', 7373)";
		String sql6 = "insert into employee11 values(7, 'Baran', 8383)";
		
		//String sql1 = "update employee11 set emp_name = 'Ranjan', salary = 5678 where id = 2 ";
		
		//String sql1 = "delete from employee11 where id = 2 ";
		
		//stmt.execute(sql1);
		
		stmt.addBatch(sql1);
		stmt.addBatch(sql3);
		stmt.addBatch(sql4);
		stmt.addBatch(sql5);
		stmt.addBatch(sql6);
		
		//stmt.executeBatch();
		
		
		String sql2 = "select * from employee11";
		
		ResultSet rs = stmt.executeQuery(sql2);
		
		while(rs.next())
		{
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" Salary : "+rs.getInt(3));
		}
		
		ResultSetMetaData rsd = rs.getMetaData();
		
		System.out.println(rsd.getColumnCount());
		
		System.out.println(rsd.getColumnName(1)+" "+rsd.getColumnClassName(1));
		
		//step 5 close the connection object
		con.close();
		
	}
}
