package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream obs = new ObjectOutputStream(fos);
		
		Employee emp = new Employee();
		
		emp.id = 111;
		emp.name = "Rohit";
		emp.city = "Vizag";
		emp.pinCode = 123123;
		
		obs.writeObject(emp);
		
		System.out.println("Done");
	}
}
