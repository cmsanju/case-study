package com.ths;

public class Exp4 implements Runnable
{

	@Override
	public void run() {
		
		System.out.println("run() "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) throws Exception
	{
		
		Exp4 t1 = new Exp4();
		//t1.start();
		
		ThreadGroup tg1 = new ThreadGroup("Bank");
		
		Thread t2 = new Thread(tg1, t1, "Credit");
		Thread t3 = new Thread(tg1, t1, "Transfer");
		Thread t4 = new Thread(tg1, t1, "Withdraw");
		
		ThreadGroup tg2 = new ThreadGroup("Maths");
		
		Thread t5 = new Thread(tg2, t1, "Add");
		Thread t6 = new Thread(tg2, t1, "Sub");
		Thread t7 = new Thread(tg2, t1, "Mul");
		
		System.out.println("Defautl thread name : "+t2.getName());
		System.out.println("Defautl thread name : "+t3.getName());
		System.out.println("Defautl thread name : "+t4.getName());
		
		
		
		System.out.println("default thread priority : "+t2.getPriority());
		System.out.println("default thread priority : "+t3.getPriority());
		System.out.println("default thread priority : "+t4.getPriority());
		
		t2.setPriority(Thread.MAX_PRIORITY);
		t4.setPriority(Thread.MIN_PRIORITY);
		
		System.out.println(" thread priority : "+t2.getPriority());
		System.out.println(" thread priority : "+t3.getPriority());
		System.out.println(" thread priority : "+t4.getPriority());
		
		t2.setName("java");
		System.out.println("After setting thread name : "+t2.getName());
		
		t2.start();
		t3.start();
		System.out.println("G1 : "+tg1.activeCount());
		
		t6.start();
		t7.start();
		System.out.println("G2 : "+tg2.activeCount());
		
		
		t2.join();
		t3.join();
		t4.join();
		
		tg1.destroy();
		System.out.println(tg1.isDestroyed());
	}
	
}
