package com.ths;

class Add
{
	public void doAdd()
	{
		System.out.println("add : "+(30+20));
	}
}

class Sub
{
	public void doSub()
	{
		System.out.println("sub : "+(44-22));
	}
}



public class Exp3 extends Thread
{
	public void run()
	{
		try
		{
			Add a = new Add();
			a.doAdd();
			Thread.sleep(2000);
			Sub b = new Sub();
			b.doSub();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		Exp3 t1 = new Exp3();
		
		System.out.println("MAX : "+MAX_PRIORITY);
		System.out.println("NORM : "+NORM_PRIORITY);
		System.out.println("MIN : "+MIN_PRIORITY);
		
		t1.start();
	}
}
