package com.ths;

public class TLCycle extends Thread
{
	@Override
	public void run()
	{
		try {
			Thread.sleep(2000);
			System.out.println("from run()");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		
		TLCycle t1 = new TLCycle();
		
		System.out.println("Before starting thread status : "+t1.isAlive());
		System.out.println("Before starting thread state : "+t1.getState());
		
		t1.start();
		System.out.println("After starting thread state : "+t1.getState());
		System.out.println("After starting thread status : "+t1.isAlive());
		
		Thread.sleep(1000);
		
		System.out.println("in sleep thread status : "+t1.isAlive());
		System.out.println("in sleep thread state : "+t1.getState());
		
		t1.join();
		
		System.out.println("after join thread status : "+t1.isAlive());
		System.out.println("after join thread state : "+t1.getState());
	}
}
