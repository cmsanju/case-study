package com.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Exp1 {
	
	public static void main(String[] args) {
		
		List<String> listNames = new ArrayList<>();
		
		listNames.add("modi");
		listNames.add("rohit");
		listNames.add("kohli");
		listNames.add("rahul");
		listNames.add("dravid");
		listNames.add("sharma");
		listNames.add("kohli");
		listNames.add("king");
		
		
		//java 8 stream api
		
		//listNames.stream().filter(x -> x.endsWith("i")).forEach(j -> System.out.println(j));
		
	List<String> unq = 	listNames.stream().distinct().collect(Collectors.toList());
	
	System.out.println(unq);
	
	
	
		//listNames.stream().sorted().forEach(j -> System.out.println(j));
		
	  //stream() sequential flow vs parallelStream(); random operations 
	}
}
