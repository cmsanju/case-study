package com.java8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Exp2 {
	
	public static void main(String[] args) {
		
		List<Employee> empData = new ArrayList<>();
		
		empData.add(new Employee(5, "Siva", 1234));
		empData.add(new Employee(3, "Kiran", 345));
		empData.add(new Employee(1, "Rakesh", 5566));
		empData.add(new Employee(4, "Rahul", 4832));
		empData.add(new Employee(2, "Ranjan", 7272));
		
		Collections.sort(empData);
		
		for(Employee k : empData)
		{
			System.out.println(k.getId()+" "+k.getName()+" "+k.getSalary());
		}
		
		Collections.sort(empData, new NameComparator());
		
		for(Employee j : empData)
		{
			System.out.println(j.getId()+" "+j.getName()+" "+j.getSalary());
		}
		
		System.out.println("=============");
		
		empData.stream().sorted(Comparator.comparing(Employee :: getId))
		.forEach(j -> System.out.println(j.getId()+" "+j.getName()+" "+j.getSalary()));
		System.out.println("=============");
		empData.stream().sorted(Comparator.comparing(Employee :: getName).thenComparing(Employee :: getSalary))
		.forEach(k -> System.out.println(k.getId()+" "+k.getName()+" "+k.getSalary()));
		
		
		Map<Integer, String> mapData = empData.stream()
				.collect(Collectors.toMap(Employee :: getId, Employee :: getName));
		
		mapData.forEach((k,v) -> System.out.println(k+" "+v));
	}
}
