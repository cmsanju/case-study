package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		
		try
		{
			System.out.println(10/0);
		}
		catch(Exception e)
		{
			//using getMessage() only displays exception message
			System.out.println(e.getMessage());
			
			//printing exception class object it will give class name and message
			System.out.println(e);
			
			//using printStackTrace() it will exception class name, message and line number
			
			e.printStackTrace();
		}
	}
}
