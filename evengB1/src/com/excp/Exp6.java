package com.excp;

public class Exp6 {
	
	//Exp9 obj = new Exp9();
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println("TEST");
			System.out.println(40/5);
			System.out.println("oK");
			
			String str = "hello";
			
			System.out.println(str.charAt(0));
			
			String s = "java";
			
			System.out.println(s.charAt(3));
			
			int[] ar = {20,30,40};
			
			System.out.println(ar[1]);
		}
		
		catch(ArithmeticException ae)
		{
			System.out.println("don't enter zero for den");
		}
		catch(NullPointerException npe)
		{
			System.out.println("enter string values");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check string length");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check array size");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		//System.out.println();
		finally
		{
			System.out.println("I AM FROM FINALLY.");
		}
	}
}
