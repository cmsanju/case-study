package com.test;	

class Product
{
	public void disp()
	{
		System.out.println("garbage collection test");
	}
	
	protected void finalize()
	{
		System.out.println("finalize() executed");
	}
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		
		Product obj = new Product();
		
		obj.disp();
		
		obj = null;
		
		System.gc();
		
		obj.disp();
	}
}
