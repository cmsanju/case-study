package com.test;

@FunctionalInterface
interface FunTest
{
	void sayHello(String msg);
	//void human();
	
	default void show()
	{
		
	}
	
	static void cat()
	{
		
	}
}

public class Exp4 {
	public static void main(String[] args) {
		
		FunTest obj = new FunTest()
				{
					public void sayHello(String msg)
					{
						System.out.println("overrided");
					}
				};
				
				obj.sayHello("hi hello");
				
				new FunTest()
				{
					public void sayHello(String msg)
					{
						System.out.println("nameless object");
						
					}
				}.sayHello("greetings...");
				
				
				FunTest obj2 = (String msg)->{
					System.out.println("lambda expression");
					
					//return "hss";
				};
				
				obj2.sayHello("greet data");
	}
}
