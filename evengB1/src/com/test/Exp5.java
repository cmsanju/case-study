package com.test;

public class Exp5 {
	
	public static void main(String[] args) {
		
		//auto boxing
		int x = 40;
		
		Integer xx = new Integer(x);
		
		double d = 33.33;
		
		Double dd = new Double(d);
		
		
		//auto un-boxing
		Float f = new Float(21.22);
		
		float ff = f;
		
		
		String s = "200";
		
		int j = Integer.parseInt(s);
		
		int k = 400;
		
		String str1 = String.valueOf(k);
		
		double l = 32.32;
		
		String str2 = String.valueOf(l);
	}
}
