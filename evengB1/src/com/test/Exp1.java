package com.test;

class P3
{
	public void draw()
	{
		System.out.println("triangle");
	}
}

class C4 extends P3
{
	@Override
	public void draw()
	{
		System.out.println("circle");
	}
	
	public void show()
	{
		
	}
}

class C5 extends P3
{
	@Override
	public void draw()
	{
		System.out.println("square");
	}
}
public class Exp1 {
	
	public static void main(String[] args) {
		
		P3 obj1 = new C4();
		
		obj1.draw();
		
		P3 obj2 = new C5();
				
		obj2.draw();	
	}
}
