package com.test;

interface Inf1
{
	float pi = 3.14f;
	
	void cat();
	void dog();
	
	private void data()
	{
		
	}
	
	//JDK 8
	
	default void greetUser()
	{
		data();
	}
	
	static void byeUser()
	{
		
	}
	
	//void laptop();
}

abstract class Abs
{
	public void human()
	{
		System.out.println("normal method");
	}
	
	public abstract void animal();
}

class Impl extends Abs implements Inf1
{
	@Override
	public void animal()
	{
		System.out.println("animal overrided");
	}
	
	@Override
	public void cat()
	{
		System.out.println("cat overrided");
	}
	
	@Override
	public void dog()
	{
		System.out.println("dog overrided");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		Impl obj = new Impl();
		
		obj.animal();
		obj.cat();
		obj.dog();
		obj.human();
		obj.greetUser();
		
		Inf1.byeUser();
	}
}
