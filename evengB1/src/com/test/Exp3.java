package com.test;

interface Inf2
{
	void add();
	
	interface Inf3{
	
	void sub();
	}
}

class Impl1 implements Inf2.Inf3
{
	@Override
	public void sub()
	{
		System.out.println("inner inf method");
	}
	
	
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		Impl1 obj = new Impl1();
		
		obj.sub();
	}
}
