package com.test;

//single level

class P1
{
	int id = 101;
	String name = "Shiva";

	public void disp()
	{
		System.out.println(id+" "+name);
	}
}

class C1 extends P1
{
	P1 obj = new P1();
	String city = "MPL";

	public void show()
	{
		System.out.println(obj.id+" "+name+" "+city);
	}
}

class C2 extends P1
{

}

public class Exp15
{
	public static void main(String[] args)
	{
	  C1 obj = new C1();

	   obj.disp();
	   obj.show();
	}
}