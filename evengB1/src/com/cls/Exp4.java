package com.cls;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class Exp4 {
	
	public static void main(String[] args) {
		
		LinkedHashSet data1 = new LinkedHashSet();
		
		data1.add(100);
		data1.add(10);
		data1.add('a');
		data1.add("java");
		data1.add(10);
		data1.add(33.45f);
		data1.add(30.23);
		data1.add(true);
		data1.add("java");
		
		System.out.println(data1);
	}
}
