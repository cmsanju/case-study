package com.cls;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Exp7 {
	
	public static void main(String[] args) {
		
		//HashMap<String, Integer> data = new HashMap<>();
		//LinkedHashMap<String, Integer> data = new LinkedHashMap<>();
		
		//TreeMap<String, Integer> data = new TreeMap<>();
		
		Hashtable<String, Integer> data = new Hashtable<String, Integer>();
		
		data.put("sony", 303);
		data.put("dell", 933);
		data.put("asus", 234);
		data.put("sony", 500);
		data.put("apple", 1234);
		data.put("stlylio", 822);
		
		System.out.println(data);
		
		Iterator<Entry<String, Integer>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println(et.getKey()+" "+et.getValue());
		}
		
		for(String key : data.keySet())
		{
			System.out.println(key+" :: "+data.get(key));
		}
		
		data.forEach((k,v)-> System.out.println(k+" <-> "+v));
		
		HashMap<Employee, Integer> empData = new HashMap<Employee, Integer>();
		
		empData.put(new Employee(101, "Rohit", "ICC"), 1);
		empData.put(new Employee(102, "Kohli", "BCCI"), 2);
		
		empData.forEach((k,v)-> System.out.println(k.getId()+" "+k.getName()+" "+k.getCmp()+" "+v));
	}
}
