package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Exp3 {
	
	public static void main(String[] args) {
		
		HashSet data1 = new HashSet();
		
		data1.add(100);
		data1.add(10);
		data1.add('a');
		data1.add("java");
		data1.add(10);
		data1.add(33.45f);
		data1.add(30.23);
		data1.add(true);
		data1.add("java");
		
		System.out.println(data1);
		
		System.out.println(data1.contains("javaaa"));
		
		System.out.println(data1.size());
		
		Iterator itr = data1.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println(data1.hashCode());
		
		HashSet data2 = new HashSet();
		
		data2.add(1000);
		data2.add('S');
		data2.add("hello");
		data2.add(292);
		data2.add("gmst");
		
		data2.addAll(data1);
		
		System.out.println(data2);
		
		System.out.println(data2.size());
	}
}
