package com.cls;

import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(100);
		data.add(10);
		data.add('a');
		data.add("java");
		data.add(10);
		data.add(33.45f);
		data.add(30.23);
		data.add(true);
		data.add("java");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("hello");
		
		System.out.println(data.peek());
		
		data.pop();
		
		System.out.println(data);
		
		System.out.println(data.search(1000));
		
		data.clear();
		
		System.out.println(data.empty());
	}
}
