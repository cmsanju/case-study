package com.cls;

import java.util.Iterator;
import java.util.TreeSet;

public class Exp5 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data = new TreeSet<>();
		
		data.add(100);
		data.add(10);
		data.add(20);
		data.add(1);
		data.add(5);
		data.add(3);
		data.add(7);
		data.add(3);
		data.add(2);
		
		System.out.println(data);
		
		TreeSet<String> ts = new TreeSet<>();
				
		ts.add("java");
		ts.add("kotlin");
		ts.add("groovy");
		ts.add("go lang");
		ts.add("android");
		ts.add("scripting");
		ts.add("angular");
		ts.add("react");
		ts.add("hibernate");
		
		System.out.println(ts);
		
		Iterator itr = ts.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		data.forEach(x -> System.out.println(x));
		
		System.out.println(ts.ceiling("xzss"));
		System.out.println(ts.subSet("go lang", "react"));
	}
}
