package com.cls;

import java.util.HashSet;
import java.util.Objects;

class Employee
{
	private int id;
	
	private String name;
	
	private String cmp;
	
	public Employee()
	{
		
	}
	
	public Employee(int id, String name, String cmp)
	{
		this.id = id;
		this.name = name;
		this.cmp = cmp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cmp, id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return Objects.equals(cmp, other.cmp) && id == other.id && Objects.equals(name, other.name);
	}
	
	
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		HashSet<Employee> empData = new HashSet<>();
		
		empData.add(new Employee(101, "Rohit", "ICC"));
		empData.add(new Employee(102, "Kohli", "BCCI"));
		empData.add(new Employee(101, "Rohit", "ICC"));
		empData.add(new Employee(103, "Hero", "BW"));
		
		empData.forEach(emp -> System.out.println(emp.getId()+" "+emp.getName()+" "+emp.getCmp()));
	}
}
