package com.cls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//Collection data = new ArrayList();
		   //List data = new ArrayList();
		  // ArrayList data = new ArrayList();
		
		   LinkedList data = new LinkedList<>();
		
		   data.add(10);
		   data.add('a');
		   data.add("java");
		   data.add(10);
		   data.add(33.45f);
		   data.add(30.23);
		   data.add(true);
		   data.add("java");
		   
		   System.out.println(data);
		   
		   System.out.println(data.size());
		   
		   System.out.println(data.get(7));
		   
		   data.set(7, "kotlin");
		   
		   System.out.println(data);
		   
		   data.remove(7);
		   
		   System.out.println(data);
		
		   //Iterator ListIterator Enumeration
		   
		   //Iterator itr = data.iterator();
		   ListIterator ltr = data.listIterator();
		   while(ltr.hasNext())
		   {
			   System.out.println(ltr.next());
		   }
		   
		   while(ltr.hasPrevious())
		   {
			   System.out.println(ltr.previous());
		   }
		   
		   //jdk 8
		   
		   data.forEach(x -> System.out.println(x));
		   
		   
		   System.out.println(data.contains("java"));
		   
		   String str = "Hi hello";
		   
		   System.out.println(str.toUpperCase());
	}
}
