package com.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloService {
	
	@GET
	@Path("/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHelloPlainText()
	{
		
		return "Hi this is simple plain text service";
	}
	
	@GET
	@Path("/html/{name}")
	@Produces(MediaType.TEXT_HTML)
	public String sayHelloHTMLResponse(@PathParam("name") String name)
	{
		String data = "This is simple URL DATA read response : "+name;
		return "<html><body><h1>Hi Hello HTML response."+data+"</h1></body></html>";
	}
}
