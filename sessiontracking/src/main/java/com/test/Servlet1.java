package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		ServletContext ctx = getServletContext();
		
		String ct =  ctx.getInitParameter("city");
		String ctr =  ctx.getInitParameter("cnt");
		
		out.println("City : "+ct+" Country : "+ctr);
		
		String dt = ct+" "+ctr;
		
		ctx.setAttribute("info", dt);
		
		out.println("<br><a href ='Servlet2'>next page</a> ");
		
		ServletConfig cfg = getServletConfig();
		
		String usr = cfg.getInitParameter("user");
		String pas = cfg.getInitParameter("pwd");
		
		out.println("<br>UserName : "+usr+" Password : "+pas);
		
		
	}

}
