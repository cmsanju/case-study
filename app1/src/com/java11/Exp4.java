package com.java11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Exp4 {
	
	static int pinCode = 123123;
	
	public static void show()
	{
		
	}
	
	public static void main(String[] args) {
		
		String str = "L&D".repeat(5);
        System.out.println(str);
        
        var list = new ArrayList<String>();
        
        List<String> obj1 = new ArrayList<>();
        
        list.add("Java");
        list.add("King");
        
        list.forEach((var s1)-> System.out.println(s1));
       
	}
	
	class Exp10
	{
		public void disp()
		{
			System.out.println(Exp4.pinCode = 44444);
		}
	}
}


