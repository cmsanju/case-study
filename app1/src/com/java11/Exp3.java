package com.java11;

public class Exp3 {
	
	public static void main(String[] args) {
		
	       String str = " JD "; 
	        System.out.print("Start");
	        System.out.print(str.strip());
	        System.out.println("End");
	        
	        System.out.print("Start");
	        System.out.print(str.stripLeading());
	        System.out.println("End");
	        
	        System.out.print("Start");
	        System.out.print(str.stripTrailing());
	        System.out.println("End");
	        
	        
	        System.out.print("Start");
	        System.out.print(str.trim());
	        System.out.println("End");
	        
	        System.out.println(str.isEmpty());
	        System.out.println(str.isBlank());
	}
}
