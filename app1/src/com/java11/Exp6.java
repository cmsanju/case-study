package com.java11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class Exp6 {
	
	public String post(String url, String data) throws IOException {
		  URL urlObj = new URL(url);
		  HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
		  con.setRequestMethod("POST");
		  con.setRequestProperty("Content-Type", "application/json");

		  // Send data
		  con.setDoOutput(true);
		  try (OutputStream os = con.getOutputStream()) {
		    byte[] input = data.getBytes(StandardCharsets.UTF_8);
		    os.write(input, 0, input.length);
		  }
		 
		  // Handle HTTP errors
		  if (con.getResponseCode() != 200) {
		    con.disconnect();
		    throw new IOException("HTTP response status: " + con.getResponseCode());
		  }

		  // Read response
		  String body;
		  try (InputStreamReader isr = new InputStreamReader(con.getInputStream());
		      BufferedReader br = new BufferedReader(isr)) {
		    body = br.lines().collect(Collectors.joining("n"));
		  }
		  con.disconnect();

		  return body;
		}
	
	public static void main(String[] args) throws Exception
	{
		
		System.out.println(new Exp6().post("https://www.tutorialspoint.com/java/index.htm", "java tutorial"));

	}
}
