package com.test;

//Java Program for the mid of element 
import java.io.*;
import java.util.LinkedList;
import java.util.ListIterator;
public class PrintMidOfLL {

 Node head;

 /*Creating a new Node*/
 class Node {
     int data;
     Node next;
     public Node(int data)
     {
         this.data = data;
         this.next = null;
     }
 }

 /*Function to add a new Node*/
 public void pushNode(int data)
 {
     Node new_node = new Node(data);
     new_node.next = head;
     head = new_node;
 }

 /*Displaying the elements in the list*/
 public void printNode()
 {
     Node temp = head;
     while (temp != null) {
         System.out.print(temp.data + "->");
         temp = temp.next;
     }
     System.out.print("Null"
                      + "\n");
 }

 /*Finding the length of the list.*/
 public int getLen()
 {
     int length = 0;
     Node temp = head;
     while (temp != null) {
         length++;
         temp = temp.next;
     }
     return length;
 }

 /*Printing the middle element of the list.*/
 public void printMiddle()
 {
     if (head != null) {
         int length = getLen();
         Node temp = head;
         int middleLength = length / 2;
         while (middleLength != 0) {
             temp = temp.next;
             middleLength--;
         }
         System.out.print("The middle element is ["
                          + temp.data + "]");
         System.out.println();
     }
 }

 public static void main(String[] args)
 {
	 /*
	 PrintMidOfLL list = new PrintMidOfLL();
     for (int i = 5; i >= 1; i--) {
         list.pushNode(i);
         list.printNode();
         list.printMiddle();
     }
     */
	 
	 LinkedList<Integer> data = new LinkedList<Integer>();
     
     data.add(1);
     data.add(2);
     data.add(3);
     data.add(4);
     data.add(5);
     
     System.out.println(data.size());
     
     System.out.println(data.get(data.size()/2));
     
     ListIterator<Integer> itr = data.listIterator();
     
     while(itr.hasNext())
     {
    	 System.out.println(itr.next());
     }
     
     while(itr.hasPrevious())
     {
    	 System.out.println(itr.previous());
     }
 }
}