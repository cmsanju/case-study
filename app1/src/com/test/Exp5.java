package com.test;

import java.util.HashMap;
import java.util.Map;

//1 class final class 
//2 private final variables
//3 no setters
//4 public constructor


final class CustomImut
{
	private final String name;
	private final String cmp;
	private final Map<String, String> mapData;
	public CustomImut(String name, String cmp, Map<String, String> mapData)
	{
		this.name = name;
		this.cmp = cmp;
		
		Map<String, String> mapObj = new HashMap<>();
		
		for(Map.Entry<String, String> et: mapData.entrySet())
		{
			mapObj.put(et.getKey(), et.getKey());
		}
		
		this.mapData = mapObj;
	}

	public String getName() {
		return name;
	}

	public String getCmp() {
		return cmp;
	}
	
	
	
	public Map<String, String> getMapData()
	{
		Map<String, String> objMap = new HashMap<>();
		
		for(Map.Entry<String, String> et :this.mapData.entrySet() )
		{
			objMap.put(et.getKey(), et.getValue());
		}
		
		return objMap;
	}
	
	
}

//without final keyword

class Imut
{
	private String cmp;
	
	public static Imut getObj( String cmp)
	{
		return new Imut(cmp);
	}
	
	private Imut(String cmp)
	{
		this.cmp = cmp;
	}

	public String getCmp() {
		return cmp;
	}
}


//custom singleton class

class SingleTon
{
	private static SingleTon obj;
	
	private SingleTon() {
		
	}
	/*
	static
	{
		try
		{
			obj = new SingleTon();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	*/
	public synchronized static SingleTon getObj()
	{
		synchronized (SingleTon.class) {
			if(obj == null)
			{
				obj = new SingleTon();
			}
		}
		
		
		return obj;
	}
}

public class Exp5 
{
	
	public static void main(String[] x) {
		
		//SingleTon obj3 = new SingleTon();
		SingleTon obj1 = SingleTon.getObj();
		
		SingleTon obj2 = SingleTon.getObj();
		
		System.out.println(obj1.hashCode()+"\n"+obj2.hashCode());
		
		System.err.print('D');//OCJP
		
		//java.io.PrintStream println and print
		//java.lang.System public static final java.io.PrintStream out;
	}
}