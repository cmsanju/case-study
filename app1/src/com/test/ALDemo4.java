package com.test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
    private String name;
    private int salary;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "name='" + name + '\'' + ", salary=" + salary + '}';
    }
}

public class ALDemo4 {
	
	public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
                new Employee("Vijay Singh", 3),
                new Employee("Amit Shah", 4),
                new Employee("Sara Khan", 5),
                new Employee("Amit Shah", 2),
                new Employee("Vijay Singh", 6),
                new Employee("Amit Shah", 7),
                new Employee("Sara Khan", 8),
                new Employee("Amit Shah", 9)
               
        );

        List<Employee> sortedEmployees = employees.stream()
                .sorted(Comparator.comparing(Employee::getName)
                        .thenComparing(Employee::getSalary))
                .collect(Collectors.toList());

        sortedEmployees.forEach(System.out::println);
        
        
        
    }
}
