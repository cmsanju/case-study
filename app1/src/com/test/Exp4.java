package com.test;

public class Exp4 {
	
	{
		System.out.println("instance");
	}
	
	static
	{
		System.out.println("static");
	}
	/*
	public static void main(String[] args) {
		
		
	        List<Employee> employees = Arrays.asList(
	                new Employee("Vijay Reddy", 30000),
	                new Employee("Amit Shah", 60000),
	                new Employee("Sara Khan", 50000),
	                new Employee("Amit Shah", 40000)
	        );

	        List<Employee> sortedEmployees = employees.stream()
	                .sorted(Comparator.comparing(Employee::getName)
	                        .thenComparing(Employee::getSalary))
	                .collect(Collectors.toList());

	        sortedEmployees.forEach(System.out::println);
	    
	}
	
	//compile time mm
	//runtime mm
	//create object
	//class loaders subsystem 
	 * 
	 */
}

