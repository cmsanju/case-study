package com.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Map<String, Integer> hm = new HashMap<>();
		
		hm.put("sony", 300);
		hm.put("asus", 345);
		hm.put("ibook", 722);
		hm.put("ideapad", 737);
		hm.put("mac", 737);
		hm.put("sony", 400);
		hm.put("asus",4949);
		
		
		System.out.println(hm);
		
		Map<Long, String> lhm = new LinkedHashMap<>();
		
		lhm.put(12341234l, "phone");
		lhm.put(47723l,"mobile");
		lhm.put(33373l, "office no");
		lhm.put(737722l, "helpline");
		lhm.put(8828222l, "customer care");
		lhm.put(8828222l, "office no");
		
		
		System.out.println(lhm);
		
		
		//1 Iterator
		
		Iterator<Entry<String, Integer>> itr = hm.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<String, Integer> et = itr.next();
			
			System.out.println("Key : "+et.getKey()+" Value : "+et.getValue());
		}
		
		//2 enhanced forLoop
		
		for(String k : hm.keySet())
		{
			System.out.println("Product : "+k+" Price : "+hm.get(k));
			
		}
		
		//3 forEach()
		
		hm.forEach((k,v)-> System.out.println("K Y : "+k+" V L : "+v));
	}
}