package com.test;

public class ArrayMinMaxAverage {  
    public static void main(String[] args) {  
        int[] numbers = {5, 10, 3, 8, 15, 2};  
  
        int max = numbers[0];  
        int min = numbers[0];  
        int sum = 0;  
  
        for (int i = 0; i < numbers.length; i++) {  
            if (numbers[i] > max) {  
                max = numbers[i];  
            }  
  
            if (numbers[i] < min) {  
                min = numbers[i];  
            }  
  
            sum += numbers[i];  
        }  
  
        double average = (double) sum / numbers.length;  
  
        System.out.println("Maximum value: " + max);  
        System.out.println("Minimum value: " + min);  
        System.out.println("Average: " + average);  
    }  
}  
