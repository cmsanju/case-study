package com.test;

import java.util.Scanner;  
  
public class PalindromeCheck {  
    public static void main(String[] args) {  
        Scanner scanner = new Scanner(System.in);  
  
        System.out.print("Enter a string: ");  
        String input = scanner.nextLine();  
  
        if (isPalindrome(input)) {  
            System.out.println("The string is a palindrome.");  
        } else {  
            System.out.println("The string is not a palindrome.");  
        }  
    }  
  
    public static boolean isPalindrome(String input) {  
        // Remove spaces and non-alphanumeric characters from the string  
        String cleanedInput = input.replaceAll("[^a-zA-Z0-9]", "");  
  
        // Convert the cleaned string to lowercase  
        String lowercaseInput = cleanedInput.toLowerCase();  
  
        // Check if the string is a palindrome  
        int left = 0;  
        int right = lowercaseInput.length() - 1;  
  
        while (left < right) {  
            if (lowercaseInput.charAt(left) != lowercaseInput.charAt(right)) {  
                return false;  
            }  
  
            left++;  
            right--;  
        }  
  
        return true;  
    }  
}  
