package com.test;

public class Exp6 {
	
	public static void main(String[] args) {
		
		int[] arr = {10,20,30,40,50};
		
		System.out.println(arr[0]);
		System.out.println(arr[1]);
		System.out.println(arr[4]);
		
		
		for(int i = 0; i<arr.length; i++)
		{
			System.out.println(arr[i]);
		}
		
		for(int x: arr)
		{
			System.out.println(x);
		}
		
		//System.out.println(arr[1]);
		
		int[] ar = new int[5];
		
		ar[0] = 50;
		ar[4] = 100;
		ar[0] = 200;
		
		
		int[][] ar1 = {{10,20,30}, {40,50,60},{70,80,90}};
		
		System.out.println(ar1[0][1]);
		
		for(int i = 0; i<=2; i++)
		{
			for(int j = 0; j<=2; j++)
			{
				System.out.print(ar1[i][j]+" ");
			}
			
			System.out.println();
		}
		
	}
}
