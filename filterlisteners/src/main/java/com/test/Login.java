package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		ServletContext ctx = getServletContext();
		
		String usr = request.getParameter("user");
		
		HttpSession session = request.getSession();
		
		session.setAttribute("info", usr);
		
		int t = (int)ctx.getAttribute("tusers");
		int c = (int)ctx.getAttribute("cusers");
		
		out.println("<h3> Total users : "+t+" Current Users : "+c+"</h3>");
		
		out.println("<br><a href ='Logout'> logout here</a> ");
	}

}
