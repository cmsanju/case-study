package com.test;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@WebListener
public class MyListener implements HttpSessionListener {

	public static int total, current;
	
	public ServletContext ctx = null;
    
    public void sessionCreated(HttpSessionEvent se)  { 
         
    	total++;
    	current++;
    	
    	ctx = se.getSession().getServletContext();
    	
    	ctx.setAttribute("tusers", total);
    	ctx.setAttribute("cusers", current);
    }

	
    public void sessionDestroyed(HttpSessionEvent se)  { 
         
    	current--;
    	
    	ctx.setAttribute("cusers", current);
    }
	
}
