package com.test;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;


@WebFilter("/Servlet1")
public class MyFilter extends HttpFilter implements Filter {
       
    

	
	private static final long serialVersionUID = 1L;


	public void destroy() {
		
		System.out.println("destroy()");
	}

	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		System.out.println("before servlet authentication");
		// pass the request along the filter chain
		chain.doFilter(request, response);
		
		System.out.println("after servlet logout");
	}

	
	public void init(FilterConfig fConfig) throws ServletException {
		
		System.out.println("init()");
	}

}
